<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(){
        $title = config('app.name', 'LyriX');
        return view('pages.index', compact('title'));
    }

    public function about(){
        $title = 'About LyriX';
        return view('pages.about', compact('title'));
    }

    public function services(){
        $title = 'Services Offered';
        $data = array(
            'title' => 'Services Offered',
            'services' => [
                'Post Lyrics',
                'Edit Posts',
                'Delete Posts'
            ]
        );
        return view('pages.services')->with($data);
    }
}
