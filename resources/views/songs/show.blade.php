@extends('layouts.app')

@section('content')
    <a href="/songs" class="btn btn-default">Go Back</a>
    <h1>{{ $song->title }}</h1>
    <small>Written on {{$song->created_at}}</small><hr>
    <h3>{{ $song->lyrics }}</h3>
@endsection