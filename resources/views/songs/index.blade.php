@extends('layouts.app')

@section('content')
    <h1>Songs</h1>
    @if (count($songs) > 0)
        @foreach ($songs as $song)
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"><a href="/songs/{{$song->id}}" style="color:white;">{{$song->title}}</a></div>
                        <div class="panel-body">
                            Written on {{$song->created_at}}
                        </div>
                    </div>
                </div>
            </div>
            {{-- <h3><a href="/songs/{{$song->id}}">{{$song->title}}</a></h3> --}}
            {{-- <small>Written on {{$song->created_at}}</small> --}}
        @endforeach
    @else
        <h2>No Songs at the Moment :(.. Start Composing!</h2>
    @endif
@endsection